
# Exercises 
 

## Variables and Operators

- Create a script file called variables.sh:
    - Set a variable named 'user'.
    - Require user to set a value in your variable.
    - Orint error if the value is not set.
    - If all is correct: print a greeting for the user.


- Create a script file called func_variables.sh:
    - Create a function that does the same as previous exercise


- Create a script file called variables_expansion.sh:
    - Require user to write down his details:
        - Name
        - Last name
        - Id
        - Address (should be array)
    - Print them back for validation:
        - Check that only first etter of each detail is CAPITAL letter.
    - If one of the details is incorrect and user would like to change them, provide that option to him.(loop it)
    - save the data in JSON format in hidden file named credentials.

    - Useful links:
        - [json](https://en.wikipedia.org/wiki/JSON)
        - [bash tool](https://devdojo.com/bobbyiliev/how-to-work-with-json-in-bash-using-jq)
        - [another link to same bash tool](https://stedolan.github.io/jq/)
        - [and another one](https://www.ultralinux.org/post/json-bash/)
        - [you get the idea](https://stackoverflow.com/questions/24942875/change-json-file-by-bash-script)



- Create a script file called func_variables_expansion.sh:
    - Write a function that does the same as exercise above.



### Setting script templates.
When working on shell scripting having template is a useful to cut down the init time.
- Create a script the will generate your shell script template.
    - Use comments, variables and printouts

### Too shame, once on you and once on me.
- Create a script that will print sentance above:
    - It should have a variable that substitue the you word with your name.
    - If that variable is not defined then it should use word `you`


### Error Handling
Implement various kinds of error handling and resource management.

An important point of programming is how to handle errors and close resources even if errors occur.

This exercise requires you to handle various errors. 
Because error handling is rather programming language specific you'll have to refer to I/O redirection 
    and `set`/`trap` commands.



### Raindrops

Convert a number to a string, the contents of which depend on the number's factors.
- If the number has 3 as a factor, output 'Pling'.
- If the number has 5 as a factor, output 'Plang'.
- If the number has 7 as a factor, output 'Plong'.
- If the number does not have 3, 5, or 7 as a factor, just pass the number's digits straight through.

Examples

```sh
    28's factors are 1, 2, 4, 7, 14, 28.
        In raindrop-speak, this would be a simple "Plong".
    30's factors are 1, 2, 3, 5, 6, 10, 15, 30.
        In raindrop-speak, this would be a "PlingPlang".
    34 has four factors: 1, 2, 17, and 34.
        In raindrop-speak, this would be "34".
```


## Positional Parameters

### Warm up
- Create a script in your $HOME/bin directory called localscript.
When the script runs, it should output information that looks as follows:

```txt
Today is Thu Dec 10 15:45:04 EST 2020.You are in /home/aschapelle and your host is vaio3.vaiolabs.com.

```
__hint__: you need to read in your current date/time, current working directory, and hostname.
Also, include comments about what the script does and indicate that the script should run with the /bin/bash shell.


### To the positions
- Create a script that prompts users for the name of the street and town where they grew up.
 - Those same variable should also be passed as positional parameters and if not,
 then to be assigned with `read` command.
- Assign town and street to variables called mytown and mystreet, and output them with
  a sentence that reads as shown in the following code 
  (of course, $mystreet and $mytown will appear with the actual town and street the user enters):

```txt
The street I grew up on was $mystreet and the town was $mytown.
```

### Back in history
- Create a script that reads in four positional parameters from the command line, 
 assigns those parameters to variables named ONE, TWO,THREE and FOUR respectively,
 and outputs that information in the following format:

```txt
There are X parameters that include Y.The first is A, the second is B, the third is C.
```

- Replace X with the number of parameters and Y with all parameters entered. 
 Then replace A with the contents of variable ONE, B with variable TWO,
 and C with variable **FOUR**. Use `shift` command bypass the variable THREE.

__hint__: use `help shift` in terminal/bash shell to see how it works.

- [Use this for help](https://devhints.io/bash)




## Looping all around

### Uncle Bem's farm

- Create a script that runs the words moose, cow, goose, and sow through a `for`  loop.
Have each of those words appended to the end of the line “I have a....” 